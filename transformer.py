import os
import sys
import pandas as pd
from rdflib import Graph, Literal, URIRef, Namespace
from rdflib.namespace import RDF
def transform_to_rdf(input_file_path):
    # Load dataset
    data = pd.read_csv(input_file_path, index_col=0, parse_dates=True, nrows = 200)
    data.drop(columns=['cet_cest_timestamp'], inplace=True)

    # Create RDF graph
    g = Graph()

    # Define namespaces
    SAREF = Namespace("https://w3id.org/saref#")
    XSD = Namespace("http://www.w3.org/2001/XMLSchema#")
    EX = Namespace("http://example.org/Frank/")

    # Bind prefixes to namespaces
    g.bind("saref", SAREF)
    g.bind("ex", EX)
    g.bind("xsd", XSD)

    def split_columnName(name):
        splits = name.split('_')
        building_Name = '_'.join(splits[:3])
        device = '_'.join(splits[3:])
        return building_Name, device

    # Iterate over each row in the dataset
    for timestamp, row in data.iterrows():
        # Iterate over each column (device) in the row
        URI_Timestamp = URIRef(EX[f'timestamps/{timestamp.isoformat()}'])

        for column, value in row.items():
            if pd.notna(value) and isinstance(value,(int,float)):
                building_name, device = split_columnName(column)

                # Define URIs for building, device, and measurement
                URI_device = URIRef(EX[f'Devices/{building_name}/{device}'])
                URI_building = URIRef(EX[f'Building/{building_name}'])
                URI_measurement = URIRef(EX[f'Measurement/{building_name}/{device}/{timestamp.isoformat()}'])

                # Add triples to the graph
                g.add((URI_building, RDF.type, SAREF.Building))
                g.add((URI_building, SAREF.Contains, URI_device))

                g.add((URI_device, RDF.type, SAREF.Device))
                g.add((URI_device, EX['has_measurement'], URI_measurement))
                g.add((URI_device, SAREF.isContainedIn, URI_building))

                g.add((URI_measurement, RDF.type, SAREF.Measurement))
                g.add((URI_measurement, SAREF.hasTimestamp, Literal(timestamp.isoformat(), datatype=XSD.dateTime)))
                g.add((URI_measurement, SAREF.isMeasuredIn, SAREF.kWH))
                g.add((URI_measurement, SAREF.hasValue, Literal(float(value), datatype=XSD.float)))
                g.add((URI_measurement, EX['is_measurement_of'], URI_device))

    # Serialize the graph to Turtle format
    output_file_path = os.path.join(os.path.dirname(input_file_path),"graph.ttl")
    g.serialize(destination=output_file_path, format="turtle")
    print(f"RDF graph has been serialized and stored in {output_file_path}")

if __name__ == "__main__":
    if len(sys.argv) != 2:
        print("Usage: python transformer.py path/to/dataset.csv")
        sys.exit(1)

    input_file_path = sys.argv[1]
    transform_to_rdf(input_file_path)
